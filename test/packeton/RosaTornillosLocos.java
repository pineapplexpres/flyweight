/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packeton;

import flyweight.Beneficiario;
import flyweight.Cotizante;
import flyweight.Factory;
import flyweight.Paciente;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisalejandropena
 */
public class RosaTornillosLocos {
    
    public RosaTornillosLocos() {
    }
    
    @Test
    public void AddExitoTioBen() {
        Factory Farc = new Factory();
        Paciente pac = new Beneficiario("Tornillos","1234","M","CALLE 85","876","Beneficiario");
        Farc.add("1234", pac);
        String a = Farc.Buscar("1234").toString();
        assertEquals(a,"Beneficiario{Nombre=Tornillos, Documento=1234, Gender=M, Direccion=CALLE 85, Telefono=876, Tipo=Beneficiario}");
        
    }
    @Test
    public void AddExitoCoti() {
        Factory Farc = new Factory();
        Paciente pac = new Cotizante("Tornillos","1234","M","CALLE 85","876","Beneficiario");
        Farc.add("1234", pac);
        String a = Farc.Buscar("1234").toString();
        assertEquals(a,"Cotizante{Nombre=Tornillos, Documento=1234, Gender=M, Direccion=CALLE 85, Telefono=876, Tipo=Beneficiario}");
        
    }
    
}
