/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flyweight;

/**
 *
 * @author luisalejandropena
 */
public class Cotizante implements Paciente {

    String Nombre;

    String Documento;

    String Gender;

    String Direccion, Telefono, Tipo;

    public Cotizante(String Nombre, String Documento, String Gender, String Direccion, String Telefono, String Tipo) {
        this.Nombre = Nombre;
        this.Documento = Documento;
        this.Gender = Gender;
        this.Direccion = Direccion;
        this.Telefono = Telefono;
        this.Tipo = Tipo;
    }

    @Override
    public String getNombre() {
        return Nombre;
    }

    @Override
    public void SetNombre(String N) {
        this.Nombre = N;
    }

    @Override
    public String getDocumento() {
        return Documento;
    }

    @Override
    public void SetDocumento(String D) {
        this.Documento = D;
    }

    @Override
    public String getGender() {
        return Gender;
    }

    @Override
    public void SetGender(String G) {
        this.Gender = G;
    }

    @Override
    public String getDireccion() {
        return Direccion;
    }

    @Override
    public void SetDireccion(String Dir) {
        this.Direccion = Dir;
    }

    @Override
    public String getTelefono() {
        return Telefono;
    }

    @Override
    public void SetTelefono(String T) {
        this.Telefono = T;
    }

    @Override
    public String getTipo() {
        return Tipo;
    }

    @Override
    public void SetTipo(String Type) {
        this.Tipo = Type;
    }

    @Override
    public String toString() {
        return "Cotizante{" + "Nombre=" + Nombre + ", Documento=" + Documento + ", Gender=" + Gender + ", Direccion=" + Direccion + ", Telefono=" + Telefono + ", Tipo=" + Tipo + '}';
    }
    
    

}
