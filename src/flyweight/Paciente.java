/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flyweight;

/**
 *
 * @author luisalejandropena
 */
public interface Paciente {

    public String getNombre();

    public void SetNombre(String N);

    public String getDocumento();

    public void SetDocumento(String D);

    public String getGender();

    public void SetGender(String G);

    public String getDireccion();

    public void SetDireccion(String Dir);

    public String getTelefono();

    public void SetTelefono(String T);

    public String getTipo();

    public void SetTipo(String Type);

}
